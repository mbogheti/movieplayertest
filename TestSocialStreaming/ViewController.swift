//
//  ViewController.swift
//  TestSocialStreaming
//
//  Created by Mida Boghetich on 18/02/16.
//  Copyright © 2016 Giangiuseppe Mida Boghetich. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController {
    var objMoviePlayerController: MPMoviePlayerController = MPMoviePlayerController()
    var urlVideo :NSURL = NSURL()
    
    
    func rotated()
    {
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            objMoviePlayerController.fullscreen = true
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            objMoviePlayerController.fullscreen = false
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        let url:NSURL = NSURL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v")!
        
//        var moviePlayer = MPMoviePlayerController(contentURL: url)
//        
//        moviePlayer.view.frame = CGRect(x: 20, y: 100, width: 200, height: 150)
//        moviePlayer.movieSourceType = MPMovieSourceType.File
//        
//        self.view.addSubview(moviePlayer.view)
//        moviePlayer.prepareToPlay()
//        moviePlayer.play()
//        
//        
        objMoviePlayerController = MPMoviePlayerController(contentURL: url)

        
        objMoviePlayerController.view.frame = CGRectMake(0, 0, 200, 200)
        
        self.view.addSubview(objMoviePlayerController.view)
        
        objMoviePlayerController.prepareToPlay()
        
        objMoviePlayerController.play()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

